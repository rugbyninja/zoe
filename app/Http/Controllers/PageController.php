<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
	/**
	 * Returns a page view based on slug
	 * @param type $slug 
	 * @return type
	 */
    public function view($slug)
    {
    	return view('view')->with('slug', $slug);
    }

    /**
     * Returns the index page
     * @return type
     */
    public function index()
    {
    	return $this->view('index');
    }
}
