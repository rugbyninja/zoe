<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product');
            $table->integer('category');
            $table->text('snippet');
            $table->text('description');
            $table->text('lush_says')->nullable();
            $table->text('options')->nullable();
            $table->decimal('rrp', 5, 2);
            $table->decimal('price', 5, 2);
            $table->boolean('featured')->default(0);
            $table->boolean('stock')->default(0);
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
